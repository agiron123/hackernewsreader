package com.example.andre.hackernewsreader;

/**
 * Created by Andre on 9/30/2014.
 */
public class Contributor {
    public String login; // GitHub username.
    public int contributions; // Commit count.
}