package com.example.andre.hackernewsreader;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Andre on 9/30/2014.
 */
public interface GitHubService {
    @GET("/repos/{owner}/{repo}/contributors")
    List<Contributor> contributors(
            @Path("owner") String owner,
            @Path("repo") String repo
    );
}