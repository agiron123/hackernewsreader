package com.example.andre.hackernewsreader;

import java.util.Date;
import java.util.List;

/**
 * Created by Andre on 9/30/2014.
 */
public class HNPageData {
    //Id of the next page to display
    int nextId;
    //The array of posts.
    HNStoryData[] items;
}
