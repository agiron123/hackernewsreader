package com.example.andre.hackernewsreader;

import android.app.Activity;
import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.converter.GsonConverter;
import java.util.List;

public class HNActivity extends ListActivity {

    TextView responseTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_list_layout);

        List<Contributor> squareContributors;

        responseTextView = (TextView) findViewById(R.id.responseTextView);
        try
        {
            AsyncTask hnTask = new HNTask().execute("http://api.ihackernews.com");
            HNPageData hackerNewsPage = (HNPageData)hnTask.get();
            HNStoryData[] stories = hackerNewsPage.items;

            ArrayList<String> titles = new ArrayList<String>();

            for(int j = 0; j < stories.length; j++)
            {
                titles.add(stories[j].title);
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,titles.toArray(new String[titles.size()]));
            setListAdapter(adapter);
        }
        catch (RetrofitError e)
        {
            responseTextView.setText(e.getMessage());
        }
        catch (Exception e)
        {
            Log.d("Exception!!", e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hn, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class HNTask extends AsyncTask<String,Integer,HNPageData>
    {
        HNPageData posts;

        @Override
        protected HNPageData doInBackground(String... strings) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(strings[0]) // The base API endpoint.
                    .build();

            HackerNewsService hackerNewsService = restAdapter.create(HackerNewsService.class);

            posts = hackerNewsService.getPageData();

            return posts;
        }

        @Override
        protected void onPostExecute(HNPageData result)
        {
        }
    }

    private class GitHubTask extends AsyncTask<String,Integer,List<Contributor>>
    {
        List<Contributor> contributorsList;

        @Override
        protected List<Contributor> doInBackground(String... strings) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(strings[0]) // The base API endpoint.
                    .build();

            GitHubService github = restAdapter.create(GitHubService.class);

            contributorsList = github.contributors("square", "okhttp");

            for (Contributor contributor : contributorsList) {
                Log.d("RetrofitOutput: ",contributor.login + " - " + contributor.contributions);
            }

            return contributorsList;
        }

        @Override
        protected void onPostExecute(List<Contributor> result)
        {
        }
    }

}