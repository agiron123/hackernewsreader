package com.example.andre.hackernewsreader;

/**
 * Created by Andre on 9/30/2014.
 */
public class HNStoryData {

    String title;
    String url;
    String id;

    int commentCount; //parse to integer after we get it
    int points;      //ditto
    String postedAgo; //exe. "2 hours ago"
    String postedBy;

    /*
    public HNStoryData(String title, String url,
                       String id, int commentCount,
                       int points, String postedAgo,
                       String postedBy)
    {
        this.title = title;
        this.url = url;
        this.id = id;
        this.postedAgo = postedAgo;
        this.postedBy = postedBy;
    }*/

}
