package com.example.andre.hackernewsreader;

import java.util.List;

import retrofit.http.GET;

/**
 * Created by Andre on 9/30/2014.
 */
public interface
        HackerNewsService {

    @GET("/page")
    HNPageData getPageData();
}
